// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Test for the staggered grid multi-component (Navier-)Stokes model
 */
#include <config.h>
#include <ctime>
#include <memory>
#include <iostream>
#include <sstream>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/timer.hh>
#include <dune/grid/io/file/dgfparser/dgfexception.hh>
#include <dune/grid/io/file/vtk.hh>
#include <dune/istl/io.hh>
#include <dumux/geometry/diameter.hh>

#include "calcitedissolutionproblem.hh"

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>

#include <dumux/common/dumuxmessage.hh>
#include <dumux/common/defaultusagemessage.hh>


#include <dumux/linear/seqsolverbackend.hh>

#include <dumux/assembly/staggeredfvassembler.hh>
#include <dumux/assembly/diffmethod.hh>
#include <dumux/nonlinear/newtonsolver.hh>

#include <dumux/discretization/method.hh>

#include <dumux/io/staggeredvtkoutputmodule.hh>
#include <dumux/io/grid/gridmanager.hh>

#include <dumux/freeflow/navierstokes/staggered/fluxoversurface.hh>


int main(int argc, char** argv) try
{
    using namespace Dumux;
    // define the type tag for this problem
    using TypeTag = Properties::TTag::DensityDrivenFlowProblem;

    // initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    // print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);

    // parse command line arguments and input file
    Parameters::init(argc, argv);

    // try to create a grid (from the given grid file or the input file)
    GridManager<GetPropType<TypeTag, Properties::Grid>> gridManager;
    gridManager.init();

    ////////////////////////////////////////////////////////////
    // run instationary non-linear problem on this grid
    ////////////////////////////////////////////////////////////

    // we compute on the leaf grid view
    const auto& leafGridView = gridManager.grid().leafGridView();

    // create the finite volume grid geometry
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    auto gridGeometry = std::make_shared<GridGeometry>(leafGridView);
    gridGeometry->update();
    
    //the problem (initial and boundary conditions)
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    auto problem = std::make_shared<Problem>(gridGeometry);

    

    // get some time loop parameters
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    const auto tEnd = getParam<Scalar>("TimeLoop.TEnd");
    const auto maxDt = getParam<Scalar>("TimeLoop.MaxTimeStepSize");
    auto dt = getParam<Scalar>("TimeLoop.DtInitial");

    // check if we are about to restart a previously interrupted simulation
    Scalar restartTime = 0;
    if (Parameters::getTree().hasKey("Restart") || Parameters::getTree().hasKey("TimeLoop.Restart"))
        restartTime = getParam<Scalar>("TimeLoop.Restart");

    // instantiate time loop
    auto timeLoop = std::make_shared<TimeLoop<Scalar>>(restartTime, dt, tEnd);
    timeLoop->setMaxTimeStepSize(maxDt);

    // the solution vector
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    const auto numDofsCellCenter = leafGridView.size(0);
    const auto numDofsFace = leafGridView.size(1);
    SolutionVector x;
    x[GridGeometry::cellCenterIdx()].resize(numDofsCellCenter);
    x[GridGeometry::faceIdx()].resize(numDofsFace);
    problem->applyInitialSolution(x);
    problem->setTimeLoop(timeLoop);
    auto xOld = x;

    // the grid variables
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    auto gridVariables = std::make_shared<GridVariables>(problem, gridGeometry);
    gridVariables->init(x);

    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using GridView = typename GridGeometry::GridView; 
    using GlobalPosition = Dune::FieldVector<Scalar, GridView::dimensionworld>;

    // intialize the vtk output module
    using IOFields = GetPropType<TypeTag, Properties::IOFields>;
    StaggeredVtkOutputModule<GridVariables, SolutionVector> vtkWriter(*gridVariables, x, problem->name());
    IOFields::initOutputModule(vtkWriter); //! Add model specific output fields
    vtkWriter.addField(problem->getDeltaRho(), "deltaRho");
    vtkWriter.addField(problem->getpH(), "pH");
    vtkWriter.addField(problem->getmCa(), "mCa");
    vtkWriter.addField(problem->getmTIC(), "mTIC");
    vtkWriter.addField(problem->getmCO3(), "mCO3");
    vtkWriter.addField(problem->getmCO3(), "mHCO3");
    vtkWriter.addField(problem->getrdiss(), "rdiss");
    vtkWriter.addField(problem->getOmega(), "Omega");
    
    // index for a cell at the center of the wall
    // Scalar wallIdx = problem->gridGeometry().numCellCenterDofs()/2 - getParam<std::vector<int>>("Grid.Cells")[0]/2;
    Scalar wallIdx = getParam<std::vector<int>>("Grid.Cells")[0] * getParam<std::vector<int>>("Grid.Cells")[1]/2;

    //output csv files initialization
    std::ofstream fout("tableDUMUX.csv");
    fout << "Time" << "," << "pH" << "," << "mCa" << "," << "mTIC" << "," << "mCO3" << "," << "mHCO3" << "," << "rdiss" << "," << "Omega" <<"\n";
    // fout << "s" << "," << "-" << "," << "mol/kg" << "," << "mol/kg" << "," << "mol/kg" << "," << "mol/kg" << "," << "mol/m^2s" << "," << "-" << "\n";

    std::vector<Scalar> pecletNumber(numDofsCellCenter);
    vtkWriter.addField(pecletNumber, "Pe");
    vtkWriter.write(0.0);

    // the assembler with time loop for instationary problem
    using Assembler = StaggeredFVAssembler<TypeTag, DiffMethod::numeric>;
    auto assembler = std::make_shared<Assembler>(problem, gridGeometry, gridVariables, timeLoop, xOld);

    // the linear solver
    using LinearSolver = Dumux::UMFPackBackend;
    auto linearSolver = std::make_shared<LinearSolver>();

    // the non-linear solver
    using NewtonSolver = Dumux::NewtonSolver<Assembler, LinearSolver>;
    NewtonSolver nonLinearSolver(assembler, linearSolver);

    // time loop
    timeLoop->start(); do
    {   
        // update pH values 
        problem->setValues(*gridVariables, x);
        // set previous solution for storage evaluations
        assembler->setPreviousSolution(xOld);
        
        // solve the non-linear system with time step control
        nonLinearSolver.solve(x, *timeLoop);

        // make the new solution the old solution
        xOld = x;
        gridVariables->advanceTimeStep();

        // advance to the time loop to the next step
        timeLoop->advanceTimeStep();

        problem->calculateDeltaRho(*gridVariables, x);
        problem->calculateTotalCO2Mass(*gridVariables, x);

        // mid boundary value to the grid (at the mid wall) was used to save the values in the csv file
        fout << timeLoop->time() << ",";
        fout << problem->getpH()[wallIdx] << ",";
        fout << problem->getmCa()[wallIdx] << ",";
        fout << problem->getmTIC()[wallIdx] << ",";
        fout << problem->getmCO3()[wallIdx] << ",";
        fout << problem->getmHCO3()[wallIdx] << ",";
        fout << problem->getrdiss()[wallIdx] << ",";
        fout << problem->getOmega()[wallIdx] << "\n";

        for (const auto& element : elements(leafGridView))
        {
            auto fvGeometry = localView(*gridGeometry);
            auto elemFaceVars = localView(gridVariables->curGridFaceVars());
            fvGeometry.bindElement(element);
            elemFaceVars.bindElement(element, fvGeometry, x);

            GlobalPosition velocity(0.0);

            for (auto&& scv : scvs(fvGeometry))
            {
                auto dofIdxGlobal = scv.dofIndex();

                for (auto&& scvf : scvfs(fvGeometry))
                {
                    auto dirIdx = scvf.directionIndex();
                    velocity[dirIdx] += 0.5*elemFaceVars[scvf].velocitySelf();
                }

                const Scalar velMag = velocity.two_norm();
                pecletNumber[dofIdxGlobal] = velMag * diameter(element.geometry()) / 2e-9;
            }
        }

        // write vtk output
        vtkWriter.write(timeLoop->time());


        // report statistics of this time step
        timeLoop->reportTimeStep();

        // set new dt as suggested by newton solver
        timeLoop->setTimeStepSize(nonLinearSolver.suggestTimeStepSize(timeLoop->timeStepSize()));
        
    } while (!timeLoop->finished());

    timeLoop->finalize(leafGridView.comm());
    fout.close();


    ////////////////////////////////////////////////////////////
    // finalize, print dumux message to say goodbye
    ////////////////////////////////////////////////////////////

    // print dumux end message
    if (mpiHelper.rank() == 0)
    {
        Parameters::print();
        DumuxMessage::print(/*firstCall=*/false);
    }

    return 0;
} // end main
catch (Dumux::ParameterException &e)
{
    std::cerr << std::endl << e << " ---> Abort!" << std::endl;
    return 1;
}
catch (Dune::DGFException & e)
{
    std::cerr << "DGF exception thrown (" << e <<
                 "). Most likely, the DGF file name is wrong "
                 "or the DGF file is corrupted, "
                 "e.g. missing hash at end of file or wrong number (dimensions) of entries."
                 << " ---> Abort!" << std::endl;
    return 2;
}
catch (Dune::Exception &e)
{
    std::cerr << "Dune reported error: " << e << " ---> Abort!" << std::endl;
    return 3;
}
catch (...)
{
    std::cerr << "Unknown exception thrown! ---> Abort!" << std::endl;
    return 4;
}
