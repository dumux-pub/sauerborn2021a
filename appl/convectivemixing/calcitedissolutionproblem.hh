// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Application where CO2 dissolves into water and creates density driven flow
 */
#ifndef DUMUX_DENSITY_FLOW_NC_TEST_PROBLEM_HH
#define DUMUX_DENSITY_FLOW_NC_TEST_PROBLEM_HH

#include <dune/grid/yaspgrid.hh>
#include <dumux/freeflow/navierstokes/problem.hh>
#include <dumux/freeflow/navierstokes/boundarytypes.hh>

#include <dumux/discretization/staggered/freeflow/properties.hh>
#include <dumux/freeflow/compositional/navierstokesncmodel.hh>

#include "co2tables.hh"
#include "h2oco2calcium.hh" // Fluidsystem

#include "findscalarroot.hh" // Brent's algorithm

namespace Dumux
{
template <class TypeTag>
class DensityDrivenFlowProblem;

namespace Properties
{

namespace TTag {
struct DensityDrivenFlowProblem { using InheritsFrom = std::tuple<NavierStokesNC, StaggeredFreeFlowModel>; };
}

// Specialize the fluid system type for this type tag
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::DensityDrivenFlowProblem>
{
    using Scalar = GetPropType<TypeTag, Scalar>;
    using BrineCO2 = FluidSystems::H20CO2Calcium<Scalar,
                    Components::TabulatedComponent<Components::H2O<Scalar>>,
                    FluidSystems::H20CO2CalciumDefaultPolicy</*simpleButFast=*/false>,
                    false >;
    using type = BrineCO2;
};

template<class TypeTag>
struct ReplaceCompEqIdx<TypeTag, TTag::DensityDrivenFlowProblem>
{ static constexpr auto value = 0; };

template<class TypeTag>
struct UseMoles<TypeTag, TTag::DensityDrivenFlowProblem>
{ static constexpr bool value = true; };

template<class TypeTag>
struct EnableGridGeometryCache<TypeTag, TTag::DensityDrivenFlowProblem>
{ static constexpr bool value = true; };

template<class TypeTag>
struct EnableGridFluxVariablesCache<TypeTag, TTag::DensityDrivenFlowProblem>
{ static constexpr bool value = true; };

template<class TypeTag>
struct EnableGridVolumeVariablesCache<TypeTag, TTag::DensityDrivenFlowProblem>
{ static constexpr bool value = true; };


// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::DensityDrivenFlowProblem>
{
    static constexpr auto dim = 2;
    using type = Dune::YaspGrid<dim, Dune::TensorProductCoordinates<double, dim> >;
};

// Set the grid type
template<class TypeTag>
struct Problem<TypeTag, TTag::DensityDrivenFlowProblem>
{
    using type = Dumux::DensityDrivenFlowProblem<TypeTag>;
};

}

/*!
 * \brief Application where CO2 dissolves into water and creates density driven flow
    \todo doc me!
 */
template <class TypeTag>
class DensityDrivenFlowProblem : public NavierStokesProblem<TypeTag>
{
    using ParentType = NavierStokesProblem<TypeTag>;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using GridView = typename GridGeometry::GridView;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;

    static constexpr auto H2OIdx = FluidSystem::H2OIdx;
    static constexpr auto volvarCO2Idx = FluidSystem::CO2Idx;
    static constexpr auto transportCO2Idx = Indices::conti0EqIdx + 1;
    static constexpr auto transportCO2EqIdx = Indices::conti0EqIdx + 1;

    static constexpr auto volvarCalciumIdx = FluidSystem::CalciumIdx;
    static constexpr auto transportCalciumIdx = Indices::conti0EqIdx + 2;
    static constexpr auto transportCalciumEqIdx = Indices::conti0EqIdx + 2;

    static constexpr auto conti0EqIdx = Indices::conti0EqIdx;

    static constexpr auto dimWorld = GridView::dimensionworld;

    using BoundaryTypes = Dumux::NavierStokesBoundaryTypes<GetPropType<TypeTag, Properties::ModelTraits>::numEq()>;
    using Element = typename GridView::template Codim<0>::Entity;
    using FVElementGeometry = typename GetPropType<TypeTag, Properties::GridGeometry>::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;

    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using SourceValues = GetPropType<TypeTag, Properties::NumEqVector>;

    using TimeLoopPtr = std::shared_ptr<TimeLoop<Scalar>>;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
    using SolVector = Dune::FieldVector<Scalar, 2>;

public:
    // Constructor
    DensityDrivenFlowProblem(std::shared_ptr<const GridGeometry> gridGeometry)
    : ParentType(gridGeometry), eps_(1e-6), storage_(0.0)
    {
        useWholeLength_ = getParam<bool>("Problem.UseWholeLength");
        xTop_ = getParam<Scalar>("Problem.MoleFractionCO2Top");

        //  initialize pH throughout the grid
        pH_ = getParam<Scalar>("Problem.pH");
        pHValues_.resize(this->gridGeometry().numCellCenterDofs(), pH_);
        mCaValues_.resize(this->gridGeometry().numCellCenterDofs());
        mTICValues_.resize(this->gridGeometry().numCellCenterDofs());
        mCO3Values_.resize(this->gridGeometry().numCellCenterDofs());
        mHCO3Values_.resize(this->gridGeometry().numCellCenterDofs());
        rdissValues_.resize(this->gridGeometry().numCellCenterDofs());
        omegaValues_.resize(this->gridGeometry().numCellCenterDofs());

        henryCoefficient_ = getParam<Scalar>("Problem.HenryCoefficient");
        temperatureGradient_ = getParam<Scalar>("Problem.TemperatureGradient", 0.0);
        temperatureInitial_ = getParam<Scalar>("Problem.Temperature");
        yLimit_ = getParam<std::vector<Scalar>>("Grid.Positions1");
        enableGravity_ = getParam<bool>("Problem.EnableGravity");
#if NOFLOW
        flowVelocity_ = 0.0;
#else
        flowVelocity_ = getParam<Scalar>("Problem.FlowVelocity");
#endif

        considerWallFriction_ = getParamFromGroup<bool>(this->paramGroup(), "Problem.ConsiderWallFriction", false);
        if (considerWallFriction_)
            height_ = getParamFromGroup<Scalar>(this->paramGroup(), "Grid.Height");

        FluidSystem::init();
        deltaRho_.resize(this->gridGeometry().numCellCenterDofs());

        fileTotalMass_.open("totalMassCO2.log", std::ios::app);
        fileTotalMass_<< "time totalMassCO2 totalMass" << std::endl;
        fileTotalMass_.close();

    }

    void setValues(const GridVariables &gridVariables, const SolutionVector& sol)
    {
        for(const auto& element : elements(this->gridGeometry().gridView()))
        {
            auto fvGeometry = localView(this->gridGeometry());
            fvGeometry.bindElement(element);

            // compute pH in the scvs
            for (const auto& scv : scvs(fvGeometry))
            {
                auto elemVolVars = localView(gridVariables.curGridVolVars());
                elemVolVars.bind(element, fvGeometry, sol);

                const auto& volVars = elemVolVars[scv];
                std::vector<Scalar> returnVector = calculateValues_(volVars, scv);
                pHValues_[scv.dofIndex()] = returnVector[0];
                mCaValues_[scv.dofIndex()] = returnVector[1];
                mTICValues_[scv.dofIndex()] = returnVector[2];
                mCO3Values_[scv.dofIndex()] = returnVector[3];
                mHCO3Values_[scv.dofIndex()] = returnVector[4];
                rdissValues_[scv.dofIndex()] = returnVector[5];
                omegaValues_[scv.dofIndex()] = returnVector[6];
            }
        }
    }

    // for writing pH values in the vtk file
    auto& getpH() const { return pHValues_; }

    auto& getmCa() const { return mCaValues_; }

    auto& getmTIC() const { return mTICValues_; }

    auto& getmCO3() const { return mCO3Values_; }

    auto& getmHCO3() const { return mHCO3Values_; }

    auto& getrdiss() const { return rdissValues_; }

    auto& getOmega() const { return omegaValues_; }

    /*!
     * \name Problem parameters
     */
    // \{

    bool shouldWriteRestartFile() const
    {
        return false;
    }

    /*!
     * \brief Return the temperature within the domain in [K].
     *
     * This problem assumes a temperature of 10 degrees Celsius.
     */
    Scalar temperature() const
    { return 273.15 + temperatureInitial_; } // 10C

    // \}
    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary control volume.
     *
     * \param globalPos The position of the center of the finite volume
     */
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {
        BoundaryTypes values;

        // Top boundary
        if(onTopBoundary_(globalPos))
        {
            values.setDirichlet(Indices::velocityXIdx);
            values.setDirichlet(Indices::velocityYIdx);
#if !NOFLOW
            values.setDirichlet(transportCalciumEqIdx);
            values.setDirichlet(transportCO2EqIdx);
#else
            values.setNeumann(transportCalciumEqIdx);
            values.setNeumann(transportCO2EqIdx);
#endif
        }

        // Bottom boundary
        else if(onBottomBoundary_(globalPos))
        {
            values.setDirichlet(Indices::pressureIdx);
#if !NOFLOW
            values.setOutflow(transportCO2EqIdx);
            values.setOutflow(transportCalciumEqIdx);
#else
            values.setNeumann(transportCalciumEqIdx);
            values.setNeumann(transportCO2EqIdx);
#endif
        }

        // Left boundary
        else if(onLeftBoundary_(globalPos))
        { 
            values.setDirichlet(Indices::velocityXIdx);
            values.setDirichlet(Indices::velocityYIdx);
            values.setNeumann(transportCO2EqIdx);
            values.setNeumann(transportCalciumEqIdx);
        }

        // Right boundary
        else
            values.setAllSymmetry();

        return values;
    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        control volume.
     *
     * \param globalPos The center of the finite volume which ought to be set.
     */
    PrimaryVariables dirichletAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables values = initialAtPos(globalPos);

        if(onTopBoundary_(globalPos)) { 
            values[Indices::velocityXIdx] = 0;
            values[Indices::velocityYIdx] = flowVelocity_;
            values[transportCalciumEqIdx] = 0;
            values[transportCO2EqIdx] = xTop_;
        }

        else if (onBottomBoundary_(globalPos)) {
            if (enableGravity_)
                values[Indices::pressureIdx] = topPressure_ + ((yLimit_[1] - yLimit_[0]) - globalPos[1])*1000.0*9.81;
            else
                values[Indices::pressureIdx] = topPressure_;
        }

        else if (onLeftBoundary_(globalPos)) {
            values[Indices::velocityXIdx] = 0;
            values[Indices::velocityYIdx] = 0;
        }

        return values;
    }

    template<class ElementVolumeVariables, class ElementFaceVariables>
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const ElementFaceVariables& elemFaceVars,
                        const SubControlVolumeFace& scvf) const
    {
        PrimaryVariables values(0.0);
        const auto& globalPos = scvf.dofPosition();
        const auto& scv = fvGeometry.scv(scvf.insideScvIdx());

        if(onLeftBoundary_(globalPos)) {
            reactionSource(scv, elemVolVars[scvf.insideScvIdx()], values);
        }
#if NOFLOW
        else if(onTopBoundary_(globalPos)){
            values[transportCalciumEqIdx] = 0;
            values[transportCO2EqIdx] = 0;
        }

        else if(onBottomBoundary_(globalPos)){
            values[transportCalciumEqIdx] = 0;
            values[transportCO2EqIdx] = 0;
        }
#endif
        return values;
    }

    /*!
     * \brief Returns whether a fixed Dirichlet value shall be used at a given cell.
     *
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry
     * \param scv The sub control volume
     */
    template<class FVElementGeometry, class SubControlVolume>
    bool isDirichletCell(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const SubControlVolume& scv,
                        int pvIdx) const
    {
        // sets a fixed pressure in one cell
        return (isLowerLeftCell_(scv) && pvIdx == Indices::pressureIdx);
    }

    /*!
     * \brief Evaluates the source term for all phases within a given
     *        sub-control volume face.
     */
    using ParentType::source;

    template<class ElementVolumeVariables, class ElementFaceVariables>
    NumEqVector source(const Element &element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const ElementFaceVariables& elemFaceVars,
                        const SubControlVolumeFace& scvf) const
    {
        auto source = NumEqVector(0.0);

        if (GridView::dimensionworld == 2 && considerWallFriction_)
        {
            static const Scalar factor = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.PseudoWallFractionFactor", 8.0);
            source[scvf.directionIndex()] = this->pseudo3DWallFriction(scvf, elemVolVars, elemFaceVars, height_, factor);

        }

        return source;
    }

    // source function for reaction terms
    template <class VolumeVariables>
    void reactionSource(const SubControlVolume& scv,
                        const VolumeVariables &volVars, 
                        NumEqVector &q) const
    {
        Scalar mH = std::pow(10, -pHValues_[scv.dofIndex()]); //[mol_H/kg_H2O] (kg_H2O OR liter_H2O)

        Scalar mCa = moleFracToMolality_(volVars.moleFraction(volvarCalciumIdx), volVars.moleFraction(volvarCalciumIdx), volVars.moleFraction(volvarCO2Idx));
        
        // calling getters to get the value mTIC
        Scalar mTIC = moleFracToMolality_(volVars.moleFraction(volvarCO2Idx), volVars.moleFraction(volvarCalciumIdx), volVars.moleFraction(volvarCO2Idx));
        
        //[mol_CO3/kg_H2O]
        Scalar mCO3 = (mTIC)/((mH * mH)/(k1_*k2_)+(mH/k2_) + 1);
        
        // compute dissolution and precipitation rate of calcite
        Scalar Omega = mCa * mCO3 / Ksp_;
        
        Scalar rdiss = 0.0;
        
        if (Omega < 1.0)
        {
            rdiss = (kdiss1_ * mH + kdiss2_) * (1 - Omega); //rdiss[mol/m²s], kdiss2[mol_Ca/m²s], kdiss1[mol_Ca/m²s]/[mol_H/kg_H2O] other part unitless! --> no special area needed!
        }

        // q[mol/m²s]
        q[conti0EqIdx] += 0;
        q[transportCO2EqIdx] -= rdiss;
        q[transportCalciumEqIdx] -= rdiss;
    }

    // \}

    /*!
     * \name Volume terms
     */
    // \{

    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * \param globalPos The global position
     */
    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables values;
        // Hydrostatic pressure
        if (enableGravity_)
            values[Indices::pressureIdx] = topPressure_ + ((yLimit_[1] - yLimit_[0]) - globalPos[1])*1000.0*9.81;
        else
            values[Indices::pressureIdx] = topPressure_;
        
        values[transportCO2EqIdx] = 2.5e-7; // assumed value; amount of TIC at the beginning
        // values[transportCO2EqIdx] = xTop_;
        values[transportCalciumEqIdx] = 0.0;
        values[Indices::velocityXIdx] = 0.0;
        values[Indices::velocityYIdx] = flowVelocity_;

        return values;
    }

    void setTimeLoop(TimeLoopPtr timeLoop)
    {
        timeLoop_ = timeLoop;
    }

    Scalar time() const
    {
        return timeLoop_->time();
    }

    void calculateTotalCO2Mass(const GridVariables& gridVariables, const SolutionVector& sol)
    {
        Scalar totalMassCO2 = 0.0;
        Scalar totalMass = 0.0;

        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
                auto fvGeometry = localView(this->gridGeometry());
                fvGeometry.bind(element);
                for (auto&& scv : scvs(fvGeometry))
                {
                    auto elemVolVars = localView(gridVariables.curGridVolVars());
                    elemVolVars.bind(element, fvGeometry, sol);

                    const auto& volVars = elemVolVars[scv];
                    totalMassCO2 += volVars.density() * volVars.moleFraction(0, 1) * scv.volume();
                    totalMass += volVars.density() * scv.volume();
                }
        }

        fileTotalMass_.open("totalMassCO2.log", std::ios::app);
        fileTotalMass_<< std::scientific << time() << " " << totalMassCO2 << " " << totalMass << std::endl;
        fileTotalMass_.close();
    }
    /*!
     * \brief Adds additional VTK output data to the VTKWriter. Function is called by the output module on every write.
     */
    void calculateDeltaRho(const GridVariables& gridVariables, const SolutionVector& sol)
    {
        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
            auto fvGeometry = localView(this->gridGeometry());
            fvGeometry.bindElement(element);
            for (auto&& scv : scvs(fvGeometry))
            {
                auto ccDofIdx = scv.dofIndex();

                auto elemVolVars = localView(gridVariables.curGridVolVars());
                elemVolVars.bind(element, fvGeometry, sol);

                deltaRho_[ccDofIdx] = elemVolVars[scv].density() - 999.694;
            }
        }
    }

    auto& getDeltaRho() const { return deltaRho_; }

private:
    const Scalar eps_;
    bool useWholeLength_;
    Scalar xTop_;
    Scalar pH_;
    std::vector<Scalar> deltaRho_;
    Scalar henryCoefficient_;
    TimeLoopPtr timeLoop_;
    Scalar storage_;
    Scalar temperatureGradient_;
    Scalar temperatureInitial_;
    std::ofstream fileTotalMass_;
    bool considerWallFriction_;
    Scalar height_;
    std::vector<Scalar> yLimit_;
    const Scalar kdiss1_{0.89}; //kgH2O/m²s = {8.9e-3} kgH2O/dm²s
    const Scalar kdiss2_{6.5e-7};//mol/m²s = {6.5e-9}mol/dm²s
    Scalar topPressure_{1.1e5};
    Scalar flowVelocity_;
    bool enableGravity_;
    const Scalar k1_{4.44631e-7};  // pow(10,-6.352)
    const Scalar k2_{4.68813e-11}; // pow(10,-10.329)
    const Scalar kw_{1e-14};
    const Scalar Ksp_{3.31131e-9}; // pow (10.,-8.48)
    std::vector<Scalar> pHValues_;

    std::vector<Scalar> mCaValues_;
    std::vector<Scalar> mTICValues_;
    std::vector<Scalar> mCO3Values_;
    std::vector<Scalar> mHCO3Values_;
    std::vector<Scalar> rdissValues_;
    std::vector<Scalar> omegaValues_;

    template <class VolumeVariables>
    std::vector<Scalar> calculateValues_(const VolumeVariables& volVars, const SubControlVolume& scv) const
    {
        Scalar mH = std::pow(10, -pHValues_[scv.dofIndex()]); // [mol_H/kg_H2O]
        
        Scalar mCa = moleFracToMolality_(volVars.moleFraction(volvarCalciumIdx),volVars.moleFraction(volvarCalciumIdx), volVars.moleFraction(volvarCO2Idx));

        // calling getters to get the value TIC and changing it to molality i.e., mTIC
        Scalar mTIC = moleFracToMolality_(volVars.moleFraction(volvarCO2Idx),volVars.moleFraction(volvarCalciumIdx), volVars.moleFraction(volvarCO2Idx));

        std::vector<Scalar> returnVector;

        if (brentAlgorithm_(mH, mCa, mTIC)){
            //[mol_CO3/kg_H2O]
            Scalar mCO3 = (mTIC)/((mH * mH)/(k1_*k2_)+(mH/k2_) + 1);

            Scalar mHCO3 = (mTIC)/(mH/k1_ + 1 + k2_/mH);
        
            // compute dissolution rate of calcite
            Scalar Omega = mCa * mCO3 / Ksp_;
        
            Scalar rdiss = 0.0;
        
            if (Omega < 1.0)
            {
                rdiss = (kdiss1_ * mH + kdiss2_) * (1 - Omega); //[mol/m²s] kdiss[mol/m²s], other part unitless! --> no special area needed!
            }
            Scalar pH = -std::log10(mH);
            // std::cout << "mH: " <<  std::setprecision(8) << std::scientific << mH << std::endl;
            returnVector.resize(7,0);
            returnVector[0] = pH;
            returnVector[1] = mCa;
            returnVector[2] = mTIC;
            returnVector[3] = mCO3;
            returnVector[4] = mHCO3;
            returnVector[5] = rdiss;
            returnVector[6] = Omega;
            return returnVector;
        }
        else
            DUNE_THROW(NumericalProblem, "Local Non-Linear solver did not converge!");
    }

    bool onLeftBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[0] < this->gridGeometry().bBoxMin()[0] + eps_; }
    
    bool onRightBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[0] > this->gridGeometry().bBoxMax()[0] - eps_; }

    bool onBottomBoundary_(const GlobalPosition& globalPos) const {
        return globalPos[1] < this->gridGeometry().bBoxMin()[1] + eps_;
    }

    bool onTopBoundary_(const GlobalPosition& globalPos) const {
        return globalPos[1] > this->gridGeometry().bBoxMax()[1] - eps_;
    }

    template<class SubControlVolume>
    bool isLowerLeftCell_(const SubControlVolume& scv) const { return scv.dofIndex() == 0; }
    
    static Scalar moleFracToMolality_(Scalar moleFracX, Scalar moleFracSalinity, Scalar moleFracCTot) {
        if(moleFracX<0)
            moleFracX=0;
        if(moleFracSalinity<0)
            moleFracSalinity=0;
        if(moleFracCTot<0)
            moleFracCTot=0;
        Scalar molalityX = moleFracX / ((1 - moleFracSalinity - moleFracCTot) * FluidSystem::molarMass(H2OIdx));
        return molalityX;
    }

    bool brentAlgorithm_(Scalar& mH, const Scalar &mCa, const Scalar &mTIC) const
    {
        static const Scalar mCorrection = -2*mCa - mH + kw_/mH + mTIC/(mH/k1_ + 1 + k2_/mH) + (2*mTIC)/((mH*mH)/(k1_*k2_)+mH/k2_ + 1);

        const auto func = [&](const Scalar x){return 2*mCa + x -kw_/x -(2*mTIC)/((x*x)/(k1_*k2_) + 1 + x/k2_) - mTIC/(x/k1_ + 1 + k2_/x) + mCorrection;};
        const auto relTol = 1e-6;
        
        // pH range 1-14
        auto brentResult = findScalarRootBrent(1e-14 /*pH 14*/, 1e-1 /*pH 1*/, func, relTol);
        if (std::get<1>(brentResult)){
            mH = std::get<0>(brentResult);
            return true;
        }
        else
            return false;
    }
}; // end class definition
} //end namespace

#endif
