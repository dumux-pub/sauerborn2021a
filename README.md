SUMMARY
=======
This is the DuMuX module containing the code for density driven flow in a column including the reaction of CO2 with calcid.

Installation
============

The easiest way to install this module and its dependencies is to create a new directory

```shell
mkdir DUMUX-CAVE && cd DUMUX-CAVE
```

and download the install script

[installnepal2020a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/nepal2020a/-/blob/master/installnepal2020a.sh)

to that folder and run the script with

```shell
chmod u+x installnepal2020a.sh
./installnepal2020a.sh
```

Installation with Docker 
========================

Create a new folder in your favourite location and change into it

```bash
mkdir DUMUX-CAVE
cd DUMUX-CAVE
```

Download the container startup script

[docker_nepal2020a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/sauerborn2021a/-/blob/master/docker/docker_nepal2020a.sh)

to that folder.

Open the Docker Container
```bash
bash docker_nepal2020a.sh open
```

Reproducing the results
=======================

After the script has run successfully, you may build the program executables

```shell
cd nepal2020a/build-cmake/appl/convectivemixing
make test_calcitedissolution
```

and run them, e.g., with

```shell
./test_calcitedissolution test_calcitedissolution.input
```
