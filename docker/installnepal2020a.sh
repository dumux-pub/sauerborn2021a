for module in common geometry grid localfunctions istl; do
  git clone -b releases/2.7 https://gitlab.dune-project.org/core/dune-$module.git
done

# dumux
git clone -b releases/3.3 https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git

# nepal2020a
git clone https://git.iws.uni-stuttgart.de/dumux-pub/nepal2020a.git

./dune-common/bin/dunecontrol --opts=dumux/cmake.opts all


